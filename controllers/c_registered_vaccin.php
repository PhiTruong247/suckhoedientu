<?php
include_once ("models/m_registered_vaccin.php");
class c_registered_vaccin {

    public function registered_vaccin () {
        $error = [];
        $m_registered_vaccin = new m_registered_vaccin();
        $m_province = $m_registered_vaccin->read_province();
        if (isset($_SESSION['email'])) {
            $email = $_SESSION['email'];
            $read_email = $m_registered_vaccin->read_email_user($email);
            if (isset($_POST['btnsubmit'])) {
                $id_vaccin = Null;
                $creator = $_SESSION['user'];
                $id_khach_hang = $read_email->id;
                $ho_ten = $_POST['ho_ten'];
                $ngay_sinh = $_POST['ngay_sinh'];
                $gioi_tinh = $_POST['gioi_tinh'];
                $can_cuoc_cong_dan = $_POST['can_cuoc_cong_dan'];
                $so_the_bao_hiem = $_POST['so_the_bao_hiem'];
                $thoi_gian_tiem = $_POST['thoi_gian_tiem'];
                $nghe_nghiep = $_POST['nghe_nghiep'];
                $so_dien_thoai = $_POST['so_dien_thoai'];
                $tinh = $_POST['tinh'];
                $quan = $_POST['quan'];
                $phuong = $_POST['phuong'];
                $t = $m_registered_vaccin->show_province($tinh);
                $q = $m_registered_vaccin->show_dictricts($quan);
                $p = $m_registered_vaccin->show_wards($phuong);
                $dia_chi = $_POST['dia_chi']. ", ". $p[0]->ten_xa_phuong. ", ". $q[0]->ten_quan_huyen. ", ". $t[0]->ten_tinh_thanh_pho;
                $cau_1 = $_POST['group1'];
                $cau_2 = $_POST['group2'];
                $cau_3 = $_POST['group3'];
                $cau_4 = $_POST['group4'];
                $cau_5 = $_POST['group5'];
                $cau_6 = $_POST['group6'];
                $cau_7 = $_POST['group7'];
                $cau_8 = $_POST['group8'];
                $cau_9 = $_POST['group9'];
                $cau_10 = $_POST['group10'];
                if (empty($error)) {
                    $result = $m_registered_vaccin->add_registered($id_vaccin,$id_khach_hang,$ho_ten,$ngay_sinh,$gioi_tinh,$can_cuoc_cong_dan,$so_the_bao_hiem,$thoi_gian_tiem,$nghe_nghiep,$so_dien_thoai,$dia_chi,$creator);
                    $question = $m_registered_vaccin->question_registered($id_vaccin,$id_khach_hang,$cau_1,$cau_2,$cau_3,$cau_4,$cau_5,$cau_6,$cau_7,$cau_8,$cau_9,$cau_10,$thoi_gian_tiem);
                    if ($result) {
                        $done[] = "Đăng kí tiêm thành công!!!";
                    }
                }  else {
                    $error[] = "Đăng kí tiêm không thành công!!!";
                }
            }
        }
        $view = "views/registered-vaccin/v_registered_vaccin.php";
        include_once ("templates/font-end/layout.php");
    }
}
?>
