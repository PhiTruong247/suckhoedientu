<?php
@session_start();
?>
<aside class="main-sidebar">
    <!-- sidebar-->
    <section class="sidebar position-relative">
        <div class="help-bt">
            <a href="https://google.com" class="d-flex align-items-center">
                <div class="bg-danger rounded10 h-50 w-50 l-h-50 text-center me-15">
                    <i data-feather="mic"></i>
                </div>
                <h4 class="mb-0">Emergency<br>help</h4>
            </a>
        </div>
        <div class="multinav">
            <div class="multinav-scroll" style="height: 100%;">
                <!-- sidebar menu-->
                <ul class="sidebar-menu pb-40" data-widget="tree">
                    <li>
                        <a href="index.php">
                            <i data-feather="monitor"></i>
                            <span>Dashboard</span>
                        </a>
                    </li>
                    <li class="treeview">
                        <a href="#">
                            <i data-feather="users"></i>
                            <span>Thông tin cá nhân</span>
                            <span class="pull-right-container">
						<i class="fas fa-angle-right"></i>
					</span>
                        </a>
                        <ul class="treeview-menu">
<!--                            <li><a href="information-user.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Nhập thông tin cá nhân</a></li>-->
                            <li><a href="patient-details.php"><i class="icon-Commit"><span class="path1"></span><span class="path2"></span></i>Thông tin cá nhân</a></li>
                        </ul>
                    </li>
                    <li>
                        <a href="information-adress.php">
                            <i data-feather="calendar"></i>
                            <span>Khai báo y tế</span>
                        </a>
                    </li>
                    <li>
                        <a href="registered-vaccin.php">
                            <i data-feather="activity"></i>
                            <span>Đăng ký tiêm</span>
                        </a>
                    </li>
                    <li>
                        <a href="ceftidication-covid.php">
                            <i data-feather="alert-triangle"></i>
                            <span>Chứng nhận tiêm chủng</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i data-feather="inbox"></i>
                            <span>Cẩm nang y tế</span>
                        </a>
                    </li>
                    <li>
                        <a href="javascript:void(0)">
                            <i data-feather="package"></i>
                            <span>Giới thiệu phần mềm</span>
                        </a>
                    </li>
                </ul>

                <div class="sidebar-widgets">
                    <div class="mx-25 mb-30 pb-20 side-bx bg-primary-light rounded20">
                        <div class="text-center">
                            <img src="https://rhythm-admin-template.multipurposethemes.com/images/svg-icon/color-svg/custom-17.svg" class="sideimg p-5" alt="">
                            <h4 class="title-bx text-primary">Make an Appointments</h4>
                            <a href="#" class="py-10 fs-14 mb-0 text-primary">
                                Best Helth Care here <i class="mdi mdi-arrow-right"></i>
                            </a>
                        </div>
                    </div>
                    <div class="copyright text-center m-25">
                        <p><strong class="d-block">Sổ sức khỏe điện tử</strong> © <script>document.write(new Date().getFullYear())</script> All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
</aside>
